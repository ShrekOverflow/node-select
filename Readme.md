Posix-Select
---

Posix style select(2) for when everything else fails and all hope is lost, an example of such an occurance is when you want to read data from a driver like `tuntaposx`, node-serialport will not work, as its not a serial port, fs.createReadStream etc also fail with IO Errors as the device is not immediately ready (BUG?).

As of now it exports a function that returns a Promise. You can call the function as

```javascript
const select = require('posix-select');
const timeout = 1000; //1 second, this is passed in ms

select({
    read: [/* Get fds from somewhere */],
    write: [/* Get fds from somewhere */],
    errors: [/* Get fds from somwhere */]
}, timeout).then((readyTo) => {
    // Descriptor is ready to read, now call `fs.read`.
    console.log(readyTo.read);
});
```

In future we may implement a poller, although I want to research why fs.createReadStream / fs.createWriteStream fail.