const select = require('.');
const fs = require('fs');
const execAsync = require('child_process').exec;
const promisify = require('util').promisify;
// Comes from tuntaposx

(async () => {
    const fd = fs.openSync('/dev/tap5', 'w+');

    try {
        const response = execAsync('ifconfig tap5 10.1.2.33 up');        
    } catch (e) {
        // console.log('Failed to run ifconfig, check are we root, are tuntapox drivers installed?');
        console.error(e);
        process.exit(-1);
    }

    try {
        let didWrite = false;
        let didRead = false;
        while(true) {
            const readyTo = await select({
                read: [fd],
                write: [fd]
            }, 1000);
            
            // console.log('Response was', readyTo);
        
            if (readyTo.read.indexOf(fd) !== -1 && !didRead){
                const mtu = 1500;
                const buff = new Buffer(mtu);
                const howMany = fs.readSync(fd, buff, 0, mtu, 0);
                // console.log("We read %d bytes : \n", howMany);
                // didRead = true;
            } else {
                // console.log("Read was not ready");
            }
    
            // if (readyTo.write.indexOf(fd) !== -1 && !didWrite) {
            //     const mtu = 1500;
            //     const buff = new Buffer(mtu);
            //     const howMany = fs.writeSync(fd, buff, 0, mtu);
            //     // console.log("We wrote %d bytes", howMany);
            //     // didWrite = true;
            // } else {
            //     // console.log("Write mode was not ready");
            // }

            if(didWrite && didRead) {
                // console.log("We succesfully read and wrote");
                break;
            }
        }
    } catch (e) {
        console.error(e);
    }
})();