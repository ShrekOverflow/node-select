#include <iostream>
#include <nan.h>
#include <memory>
#include <sys/select.h>

// @TODO: Probably optimize this code as of now we are wasting both
// Memory and CPU Cycles.

// extern "C" {
//     int select(int, fd_set *, fd_set *, fd_set *, timeval *);
// }

namespace nodeposixselect
{

using std::vector;
using v8::Function;
using v8::FunctionTemplate;
using v8::FunctionCallbackInfo;
using v8::Isolate;
using v8::Local;
using v8::Object;
using v8::String;
using v8::Value;
using v8::Array;
using v8::Number;

using Nan::AsyncQueueWorker;
using Nan::AsyncWorker;
using Nan::Callback;
using Nan::HandleScope;
using Nan::New;
using Nan::To;

class AsyncSelect : public AsyncWorker
{
  public:
    AsyncSelect(Callback *callback, vector<int> infds, vector<int> outfds, vector<int> errorfds, int tInMs)
        : AsyncWorker(callback), infds(infds), outfds(outfds), errorfds(errorfds), tInMs(tInMs)
    {
    }
    ~AsyncSelect() {}

    //** Call selekt here.
    void Execute()
    {

        std::unique_ptr<fd_set> rfds(new fd_set);
        std::unique_ptr<fd_set> wfds(new fd_set);
        std::unique_ptr<fd_set> efds(new fd_set);

        FD_ZERO(rfds.get());
        FD_ZERO(wfds.get());
        FD_ZERO(efds.get());

        int maxFd = 0;

        for (auto const &rfd : this->infds)
        {
            FD_SET(rfd, rfds.get());
            if (rfd > maxFd)
            {
                maxFd = rfd;
            }
        }

        for (auto const &wfd : this->outfds)
        {
            FD_SET(wfd, wfds.get());
            if (wfd > maxFd)
            {
                maxFd = wfd;
            }
        }

        for (auto const &efd : this->errorfds)
        {
            FD_SET(efd, efds);
            if (efd > maxFd)
            {
                maxFd = efd;
            }
        }

        std::unique_ptr<timeval> timeout(nullptr);
        
        if (this->tInMs != 0) {
            timeout = std::unique_ptr<timeval>(new timeval);
            timeout->tv_sec = this->tInMs / 1000;
            timeout->tv_usec = (this->tInMs % 1000) * 1000;                
        }

        
        this->retVal = select(maxFd + 1, rfds.get(), wfds.get(), efds.get(), timeout.get());

        if (retVal < 0)
        {
            this->retVal = errno;
            return;
        }

        for (auto const &rfd : this->infds)
        {
            if (FD_ISSET(rfd, rfds.get()))
            {
                this->readyInfds.push_back(rfd);
            }
        }

        for (auto const &wfd : this->outfds)
        {
            if (FD_ISSET(wfd, wfds.get()))
            {
                this->readyOutfds.push_back(wfd);
            }
        }

        for (auto const &efd : this->errorfds)
        {
            if (FD_ISSET(efd, efds.get()))
            {
                this->readyErrorfds.push_back(efd);
            }
        }
    }

    void HandleOKCallback()
    {
        HandleScope scope;

        if (this->retVal < 0)
        {
            Local<Value> argv[] = {
                Nan::ErrnoException(retVal)};

            callback->Call(1, argv);
            return;
        }

        Local<Array> changedReadFDs = Nan::New<Array>(0);
        Local<Array> changedWriteFDs = Nan::New<Array>(0);
        Local<Array> changedErrorFDs = Nan::New<Array>(0);

        for (uint32_t i = 0; i < this->readyInfds.size(); i++)
        {
            changedReadFDs->Set(i, Nan::New<Number>(this->readyInfds[i]));
        }

        for (uint32_t i = 0; i < this->readyOutfds.size(); i++)
        {
            changedWriteFDs->Set(i, Nan::New<Number>(this->readyOutfds[i]));
        }

        for (uint32_t i = 0; i < this->readyErrorfds.size(); i++)
        {
            changedErrorFDs->Set(i, Nan::New<Number>(this->readyErrorfds[i]));
        }

        Local<Value> argv[] = {
            Nan::Null(),
            changedReadFDs,
            changedWriteFDs,
            changedErrorFDs
        };

        callback->Call(4, argv);
    }

  private:
    vector<int> infds;
    vector<int> outfds;
    vector<int> errorfds;

    vector<int> readyInfds;
    vector<int> readyOutfds;
    vector<int> readyErrorfds;

    int tInMs;
    int retVal;
};

NAN_METHOD(SelectAsync)
{
    vector<int> infds(0);
    vector<int> outfds(0);
    vector<int> errorfds(0);

    Local<Array> readFdsToWatch = Local<Array>::Cast(info[0]);
    Local<Array> writeFdsToWatch = Local<Array>::Cast(info[1]);
    Local<Array> errorFdsToWatch = Local<Array>::Cast(info[2]);

    for (size_t i = 0; i < readFdsToWatch->Length(); i++)
    {
        infds.push_back(To<int>(readFdsToWatch->Get(i)).FromJust());
    }

    for (size_t i = 0; i < writeFdsToWatch->Length(); i++)
    {
        outfds.push_back(To<int>(writeFdsToWatch->Get(i)).FromJust());
    }

    for (size_t i = 0; i < errorFdsToWatch->Length(); i++)
    {
        errorfds.push_back(To<int>(errorFdsToWatch->Get(i)).FromJust());
    }

    int tInMs = To<int>(info[3]).FromJust();

    Callback *callback = new Callback(info[4].As<Function>());

    AsyncQueueWorker(new AsyncSelect(callback, infds, outfds, errorfds, tInMs));
}

NAN_MODULE_INIT(InitAll)
{
    Nan::Set(target, New<String>("selectAsync").ToLocalChecked(),
             Nan::GetFunction(New<FunctionTemplate>(SelectAsync)).ToLocalChecked());
}

NODE_MODULE(addon, InitAll)
}