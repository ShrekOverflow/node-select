const binding = require('./build/Release/addon.node');

module.exports = function select(fds, timeout){
    const readFds = fds.read || [];
    const writeFds = fds.write || [];
    const errorFds = fds.error || [];

    // Allow passing 0 to wait forever!
    if (timeout === undefined) {
        timeout = 10000;
    }

    return new Promise((resolve, reject) => {
        binding.selectAsync(readFds, writeFds, errorFds, timeout, 
            (err, readyReadFDs, readyWriteFds, readyErrorFds) => {
                if (err) {
                    return reject(err);
                }
                resolve({
                    read: readyReadFDs || [],
                    write: readyWriteFds || [],
                    errors: readyErrorFds || [],
                });
            }
        );
    });
};